# Como colocar sua foto como participante

1. Faça um Fork do repositório.
2. Clone o repositório do GitLab para sua máquina local: `git clone https://gitlab.com/allythy/minicurso-git-gitlab.git`
3. Entre na pasta que foi clonada: `cd minicurso-git-gitlab`
4. Crie uma branch para adicionar sua foto: `git checkout -b <nome_participante>`
5. Entre na pasta `templates/users`: `cd templates/users`
6. Copie o arquivo `template.html` para nome_do_participante.html: `cp template.html nome_do_participante.html`
7. Altere o arquivo que você copiou agora(nome_do_participante.html). Colocando os deus dados:nome, foto, link do do seu GitLab.
8. Adicione o arquivo novo: `git add .`
9. Commit suas alterações: `git commit -m "Novo participante Allythy Rennan"`
10. Mande o Push para sua branch: `git push origin <nome_participante>`
11. Entre no GitLab e envie o seu Merge Request

## Rodando o projeto localmente

Primeiro, temos que criar o nosso ambiente virtual:

```
virtualenv env
```

Ativando o ambiente virtual:

```
source env/bin/activate
```

Instalando as dependências do projeto:

```
pip install -r requirements.txt
```

Rodando a aplicação:

```
python app.py
```

OBS: Se você não souber instalar o pip no Debian e derivados eu fiz um post ensinando como fazer, [você pode ler aqui.](https://allythy.github.io/Como-instalar-o-pip-para-gerenciar-pacotes-do-Python-no-GNU-Linux)
